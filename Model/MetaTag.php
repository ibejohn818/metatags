<?php
App::uses("MetaTagsAppModel","MetaTags.model");
class MetaTag extends MetaTagsAppModel {


  public function __construct($id = false, $table = null, $ds = null) {  
    
    $relations = Configure::read("MetaTags.MetaTag.relations");
    
    $this->hasMany = $relations['hasMany'];
    $this->belongsTo = $relations['belongsTo'];
    $this->hasAndBelongsToMany = $relations['hasAndBelongsToMany'];
    $this->hasOne = $relations['hasOne'];

     parent::__construct($id,$table,$ds);

  }

  public function addTag($metaKey,$metaValue) {

    //check to see if the tag already exists
    $chk = $this->find('first',array(
      'conditions'=>array(
        'MetaTag.meta_key'=>$metaKey,
        'MetaTag.meta_value'=>$metaValue
      ),
        'fields'=>array('MetaTag.id')
    ));

    if(isset($chk['MetaKey']['id'])) {
  
       return $chk['MetaKey']['id'];

    }
  
    $this->save(array(
        'meta_key'=>$metaKey,
        'meta_value'=>$metaValue
    ));
  
    return $this->id;

  }

  public function attachTagToModel($model,$foreignKey,$meta_tag_id,$sortWeight = 99) {

    $sdata = array(
      'model'=>$model,
      'foreignKey'=>$foreignKey,
      'meta_tag_id'=>$meta_tag_id,
      'sort_weight'=>$sortWeight
    );

    $this->MetaKeyModel->save($sdata);

    return $this->MetaKeyModel->id;

  }

}
