<?php

//Model Relationships
Configure::write("MetaTags.MetaTag.relations",array(
  'hasMany'=>array(
    "MetaTagModel"=>array(
      'className'=>"MetaTags.MetaTagModel"
    )
  ),
  'hasAndBelongsToMany'=>array(),
  'belongsTo'=>array(),
  'hasOne'=>array()
));
Configure::write("MetaTags.MetaTagModel.relations",array(
  'hasMany'=>array(),
  'hasAndBelongsToMany'=>array(),
  'belongsTo'=>array(
    "MetaTag"=>array(
      'className'=>"MetaTags.MetaTag"
    )
  ),
  'hasOne'=>array()
));
