<div class="page-header">
  <h1>Edit Meta Tag</h1>
</div>
<?php
echo $this->Form->create(
  'MetaTag',
  array(
    'url'=>$this->here
  )
);
?>
<div class="edit">
<?php

echo $this->Form->input('id');
echo $this->Form->input('meta_key');
echo $this->Form->input('meta_value');

?>
</div>
<div class="form-actions">
<button class="btn btn-primary">Update Meta Tag</button>
</div>
<?php echo $this->Form->end(); ?>
