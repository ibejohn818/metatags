<div class="page-header">
  <h1>Meta Tags</h1>
</div>
<div class="index">
<table class='table table-striped table-bordered'>
    <thead>
        <tr>
          <th>
          <?php echo $this->Paginator->sort('id'); ?>
        </th>
        <th>
           <?php echo $this->Paginator->sort('created'); ?>
        </th>
        <th>
          <?php echo $this->Paginator->sort('meta_key'); ?>
        </th>
        <th>
          <?php echo $this->Paginator->sort('meta_value'); ?>
        </th>
         <th>
          -
          </th>
        </tr>
  </thead>
    <?php foreach($tags as $k=>$v): ?>
  <tbody>
    <tr>
    <td><?php echo $v['MetaTag']['id']; ?></td>
      <td>
          <?php echo $v['MetaTag']['created']; ?>
      </td>
      <td>
          <?php echo $v['MetaTag']['meta_key']; ?>
      </td>
      <td>
         <?php echo $v['MetaTag']['meta_value']; ?>
      </td>
      <td class='actions'>

        <?php
      
          $editUrl = $this->Html->url(array(
            'action'=>"edit",
            $v['MetaTag']['id']
          ));

        ?>
          <a class='btn btn-primary btn-sm' href="<?php echo $editUrl; ?>">Edit</a>
      </td>
    </tr>
  </tbody>
  <?php endforeach; ?>
</table>
</div>
