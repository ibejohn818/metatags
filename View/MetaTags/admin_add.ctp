<div class="page-header">
  <h1>Add Meta Tag</h1>
</div>
<?php
echo $this->Form->create("MetaTag",array(
"url"=>$this->here
));
?>
<div class="add form">
<?php

echo $this->Form->input('meta_key'); 

echo $this->Form->input('meta_value'); 

?>
</div>
<div class="form-actions">
<button class='btn btn-primary'>
Add New Meta Tag
</button>
</div>
<?php echo $this->Form->end(); ?>
