<?php

App::uses("MetaTagsAppController","MetaTags.Controller");

class MetaTagsController extends MetaTagsAppController {

    public $uses = array(
      "MetaTags.MetaTag"
    );

    public function beforeFilter() {

       parent::beforeFilter();

    }

    public function admin_index() {

      $this->Paginator->settings = array(
        'order'=>array(
          "MetaTag.id"=>"DESC"
        )
      );

      $tags = $this->paginate("MetaTag");
  
      $this->set(compact("tags"));

    }

    public  function admin_add() {

      if($this->request->is('post') || $this->request->is('put')) {

          if($this->MetaTag->addTag($this->request->data['MetaTag']['meta_key'],$this->request->data['MetaTag']['meta_value'])) {
          
              $this->Session->setFlash("Meta Tag Added Successfully");
              
              $this->redirect(array(
                "action"=>"index",
              ));

          }

      }

    }
    
    public function admin_edit($id) {

      if($this->request->is('post') || $this->request->is('put')) {
  
          $this->MetaTag->create();
          $this->MetaTag->id = $this->request->data['MetaTag']['id'];
  
        if($this->MetaTag->save($this->request->data)) {

          $this->Session->setFlash("Meta Tag Updated Successfully");
  
          $this->redirect(array(
            
              'action'=>"index"
          
          ));

        }

      } else {
      
        $this->request->data = $this->MetaTag->find('first',array(
        
          'conditions'=>array(
            'MetaTag.id'=>$id
           ),
            'contain'=>false

        ));
      
      }

    }
}
